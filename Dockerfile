FROM docker.io/library/ruby:3.0.0

RUN apt-get update -qq && apt-get install -y build-essential

RUN apt-get install -y libxml2-dev libxslt1-dev

# Install node from nodesource
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get install -y nodejs

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -  && \
    echo "deb http://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && \
    apt-get update && apt-get install -y yarn

ENV APP_HOME /opt/measuR
WORKDIR /opt/measuR

ENV BUNDLE_PATH $APP_HOME/vendor
RUN mkdir -p $BUNDLE_PATH

ADD Gemfile* $APP_HOME/
RUN bundle install

ADD package.json postcss.config.js babel.config.js yarn.lock Rakefile config.ru $APP_HOME/
ADD app $APP_HOME/app
ADD bin $APP_HOME/bin
ADD public $APP_HOME/public
ADD config $APP_HOME/config

RUN mkdir node_modules
RUN yarn install --check-files
RUN bundle exec rake assets:precompile

RUN mkdir -p db tmp log && chown -R 7654:7654 db tmp log
USER 7654
CMD bin/rails server
