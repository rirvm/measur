#!/bin/bash
cd "$(dirname "$0")"

CR=docker
which $CR &>/dev/null || CR=podman

$CR build --network host -t registry.gitlab.com/rirvm/measur .
$CR push registry.gitlab.com/rirvm/measur
