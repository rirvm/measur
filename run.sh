#!/bin/bash
cd "$(dirname "$0")"

CR=docker
which $CR &>/dev/null || CR=podman

$CR run --rm --name measuR \
  -e GITLAB_TOKEN="$GITLAB_TOKEN" \
  -e RAILS_ENV=development \
  -e RAILS_LOG_TO_STDOUT=1 \
  -e RAILS_SERVE_STATIC_FILES=1 \
  -e SECRET_KEY_BASE=asdfasdf \
  -v .:/opt/measuR:U \
  --net host \
  -it \
  registry.gitlab.com/rirvm/measur $1

sudo chown -R user:user .
