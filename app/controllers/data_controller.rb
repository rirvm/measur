class DataController < ApplicationController
  include Artifacts
  include BenchmarkData

  def job
    @id = params[:job]
    job = gitlab_artifact(@id)

    sel = nil
    if selections.include?(params[:selection])
      sel = selections[params[:selection]]
    end
    if params[:execution].present? && params[:execution] != 'all'
      sel[:include_execution] = Integer(params[:execution])
    end
    warmup = 5
    if params[:warmup].present?
      warmup = Integer(params[:warmup])
    end

    res = job.merge(res: process_rebench_data(job[:data], warmup, sel))
    info = res[:info]

    filtered_info = {
      job_id: info['id'],
      name: artifact_job_names[info['name'].to_sym],
      ref: info['ref'],
      started_at: info['started_at'],
      duration: info['duration'],
      commit: info['commit']['id'],
      commit_short: info['commit']['short_id'],
      commit_message: info['commit']['message'],
      pipeline: info['pipeline']['web_url'],
    }
    name = "#{filtered_info[:name]}-#{filtered_info[:commit]}"

    full_res = {
      info: filtered_info,
      data: res[:res]
    }
    data_res = []
    res[:res].each do |suite, bms|
      bms.each do |name, bm|
        bm[:cold].each do |res|
          data_res << [filtered_info[:name], filtered_info[:commit], suite, name, res, 0]
        end
        bm[:warm].each do |res|
          data_res << [filtered_info[:name], filtered_info[:commit], suite, name, res, 1]
        end
      end
    end
    csv = "vm,commit,suite,benchmark,ms,warm\n"+
      data_res.map{|d| d.join(",")}.join("\n") + "\n"

    respond_to do |format|
      format.data { send_data job[:raw], filename: "#{name}.data" }
      format.csv  { send_data csv, filename: "#{name}.csv" }
      format.json { send_data full_res.to_json, filename: "#{name}.json" }
      format.yaml { send_data full_res.to_yaml, filename: "#{name}.yaml" }
    end
  end
end
