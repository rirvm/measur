require 'zip'
require  'open-uri'


module Artifacts
  include ActiveSupport::Concern

  def artifacts_path
     Rails.root.join('tmp','cache','artifacts')
  end

  def artifact_job_names
    {
      benchmark_llvm:  'Ř',
#      benchmark_pir:  'Ř-interp',
#      benchmark_rir:  'Ř-noopt',
      benchmark_fastr: 'fastr',
      benchmark_gnur:  'gnur',
#      benchmark_llvm_ginger:  'Ř-ginger',
#      benchmark_pir_ginger:  'Ř-interp-ginger',
#      benchmark_rir_ginger:  'Ř-noopt-ginger',
#      benchmark_fastr_ginger: 'fastr-ginger',
#      benchmark_gnur_ginger:  'gnur-ginger',
    }
  end

  def gitlab_token
    @@token ||= ENV['GITLAB_TOKEN']
  end

  def gitlab_project
    10979413
  end

  def gitlab_url(path)
    "https://gitlab.com/api/v4/projects/#{gitlab_project}/#{path}"
  end

  def gitlab_get(adr)
    res = Faraday.get adr do |get|
      get.headers['PRIVATE-TOKEN'] = gitlab_token
    end
  end

  def gitlab_artifacts_info_fetch(commit, type)
    options = {skip_nil: true}
    if commit.date > 2.days.ago
      options[:expires_in] = 10.minutes
    end
    Rails.cache.fetch("gitlab_artifact_info_fetch_#{commit}_#{type}", options) do
      logger.info "Fetching #{commit} #{type} #{commit.date}"
      adr = gitlab_url("repository/commits/#{commit.sha}/statuses?name=#{type}")
      res = gitlab_get(adr)
      return nil unless res.status == 200
      res = JSON.parse(res.body)
      res.select do |v|
        v['status'] == 'success'
      end
    end
  end

  def gitlab_artifacts_info(commit, types)
    res = types.map do |type|
      r = gitlab_artifacts_info_fetch(commit, type)
      [type, r.last] if r
    end.compact
    Hash[res]
  end

  def gitlab_artifact(job)
    FileUtils.mkdir(artifacts_path) unless File.directory?(artifacts_path)

    zip_name = File.join(artifacts_path, "artifact_#{job}.zip")
    if !File.exists?(zip_name) || File.empty?(zip_name)
      logger.info "Getting artifact info for #{job}"
      adr = gitlab_url("jobs/#{job}/artifacts")
      logger.info "Getting artifact at url: #{adr}"
      
      res = gitlab_get(adr)
      loc = res.headers[:location]
      logger.info "Getting artifact for #{job} from #{loc}"

      download = URI.open(loc)
      IO.copy_stream(download, zip_name)
      
    end
    name = File.join(artifacts_path, "artifact_#{job}.data")
    if !File.exists?(name) || File.empty?(name)
      logger.info "Unzipping artifact for #{job}"
      res = Zip::File.open(zip_name) do |io|
        data = io.get_entry("benchmarks.data")
        data.get_input_stream.read
      end
      if res
        File.write(name, res)
      else
        File.unlink(zip_name)
      end
    end
    Rails.cache.fetch("gitlab_artifact_#{job}", skip_nil: true) do
      logger.info "Fetching job #{job} info"
      if info = (JSON.parse(gitlab_get(gitlab_url("jobs/#{job}")).body).merge(job_id: job) rescue nil)
        data_raw = File.read name
        data = data_raw.split("\n").reject do |l|
          l =~ /#.*/
        end.map do |l|
          l.split("\t")
        end.select do |l|
          l[3] == "ms"
        end

        {info: info, data: data, raw: data_raw}
      end
    end
  end
end
