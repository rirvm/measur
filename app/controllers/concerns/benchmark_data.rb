module BenchmarkData
  include ActiveSupport::Concern

  def selections
    {
      "all" => nil,
      "main" => {exclude_suite_regex: /(_annotations$|^simple$)/},
      "major" => {
        include_benchmark: {
          "are-we-fast-r" => ["Mandelbrot", "Storage", "Bounce"],
          "real_thing" => ["convolution", "flexclust"],
          "shootout" => ["fastaredux","fasta",
                         "fannkuchredux","knucleotide",
                         "nbody","mandelbrot_ascii",
                         "binarytrees","spectralnorm",
                         "reversecomplement","regexdna","pidigits"],
        },
      },
      "promisebreaker" => {
        include_benchmark: {
          "are-we-fast-r" => ["Mandelbrot", "Storage", "Bounce"],
          "real_thing" => ["flexclust"],
          "shootout" => ["fastaredux","fasta",
                         "fannkuchredux","knucleotide",
                         "nbody",
                         "binarytrees","spectralnorm",
                         "reversecomplement","regexdna","pidigits"],
        },
      },
    }
  end

  def mean(results)
    results.map{|r| Float(r)}.inject(:+) / results.size
  end

  def median(results)
    sorted = results.map{|r| Float(r)}.sort
    if sorted.size == 1
      return sorted.first
    elsif sorted.size % 2 == 1
      sorted[sorted.size/2]
    else
      (sorted[sorted.size/2 - 1] + sorted[sorted.size/2]) / 2.0
    end
  end

  def gmean(x)
    prod=1.0
    x.each {|v| prod *= v}
    prod**(1.0/x.size)
  end

  def variance(results)
    m = mean(results)
    Math.sqrt(mean(results.map{|r| v = Float(r)-m; v*v}))
  end

  def all_executions(data)
    data.map do |d|
      execution = Integer(d[0])
    end.uniq
  end

  def process_rebench_data(data, warmup, selection=nil)
    results = {}

    data.each do |d|
      name = d[5]
      suite = d[7].chomp("_reduced").chomp("_extra")
      time = d[2]
      iter = Integer(d[1])
      execution = Integer(d[0])
      next unless d[3] == "ms"

      if selection
        if (e = selection[:exclude_suite_regex])
          next if suite =~ e
        end
        if (e = selection[:include_benchmark])
          next unless (e[suite]||[]).include?(name)
        end
        if (e = selection[:include_execution])
          next unless execution == e
        end
      end

      results[suite] ||= {}
      results[suite][name] ||= {cold: [], warm: [], all: []}
      if iter > warmup
        results[suite][name][:warm] << time
      else
        results[suite][name][:cold] << time
      end
      results[suite][name][:all] << time
    end

    results.keys.each do |suite|
      results[suite].keys.each do |name|
        res = results[suite][name]
        next if res[:warm].empty?
        results[suite][name][:mean] = mean(res[:warm])
        results[suite][name][:median] = median(res[:warm])
        results[suite][name][:variance] = variance(res[:warm])
      end
    end

    results
  end

  def diff_rebench_data(a, b)
    res = {}
    b.keys.each do |n|
      res[n] = :missing if !a[n]
    end
    a.keys.each do |n|
      if !b[n] || !a[n][:mean] || !b[n][:mean]
        res[n] = :missing
        next
      end
      res[n] = {
        mean:   b[n][:mean]   / a[n][:mean],
        median: b[n][:median] / a[n][:median],
      }
    end
    res2 = {
      res: res.sort do |a,b|
        (a[1] == :missing ? -1000 : a[1][:mean]) <=>
          (b[1] == :missing ? -1000 : b[1][:mean])
      end,
    }
    means = res.map{|k, v| v[:mean] unless v == :missing}.compact
    if means.empty?
      return nil
    end
    res2[:mean] = gmean(means)
    medians = res.map{|k, v| v[:median] unless v == :missing}.compact
    res2[:median] = gmean(medians)
    res2
  end
end
