module Repository
  include ActiveSupport::Concern

  def repo_conf
    @conf ||= begin
      dir = Rails.root.join('tmp','cache','repos')
      FileUtils.mkdir_p(dir) unless File.directory?(dir)
      {
        name: 'rir',
        url: 'https://github.com/reactorlabs/rir.git',
        storage: dir,
        lock: File.join(dir, 'rir.lock'),
        internal_lock: File.join(dir, 'rir_intenal.lock'),
      }
    end
  end

  def git_repo
    Filelock repo_conf[:internal_lock] do
      unless File.directory? File.join(repo_conf[:storage], 'rir')
        Git.clone repo_conf[:url], repo_conf[:name], path: repo_conf[:storage]
      end
      @repo ||= Git.open(File.join(repo_conf[:storage], repo_conf[:name]))
      Rails.cache.fetch("update_repo", expires_in: 5.minutes) do
        logger.info "updating repo"
        @repo.fetch
      end
    end
    @repo
  end

  def update_repo
    git_repo.pull
  end

  def git_remote_branches
    git_repo.branches.map{|b| b.full}.select{|n| n =~ /origin/}.reject{|b| b =~ /HEAD/}
  end
end
