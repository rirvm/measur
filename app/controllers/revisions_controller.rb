class RevisionsController < ApplicationController
  include Repository
  include Artifacts

  def index
    add_filtered_param!(:branch, git_remote_branches, 'remotes/origin/master')
    add_filtered_param!(:page, :int, 0)
    add_filtered_param!(:per_page, :int, 20)
    add_filtered_param!(:vmset, ['rsh', 'legacy', 'all'], 'rsh')
    add_filtered_param!(:filter, ['all', 'merges'], 'all')
    return if enforce_params!

    @branches = git_remote_branches

    hashes = []
    Filelock repo_conf[:lock] do
      git_repo.checkout(@branch)
      if @filter == 'merges'
        hashes = git_repo.log((@page+1) * (@per_page * 100))
                         .select{|c| c.message =~ /Merge pull request/}
      else
        hashes = git_repo.log(@per_page*(@page+1))
      end
      hashes = hashes[(@per_page*@page) .. (@per_page*(@page+1))]
    end

    @vmset_items = case @vmset
            when 'rsh'
              {artifact_job_names.keys.first => artifact_job_names.values.first}
            when 'all'
              Hash[artifact_job_names.reject{|n| n =~ /ginger/}]
            else
              artifact_job_names
            end

    @log = Hash[hashes.map do |c|
      jobs = gitlab_artifacts_info(c, @vmset_items.keys)
      [c, Hash[
          jobs.map do |n, v|
            [n, v['id']] if v && v.any?
          end.compact]]
    end]
  end
end
