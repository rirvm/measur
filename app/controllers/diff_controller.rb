class DiffController < ApplicationController
  include BenchmarkData
  include Artifacts

  helper_method :selections

  def warmups
    [0,1,3,5,8,10]
  end
  helper_method :warmups

  def show
    add_filtered_param!(:warmup, :int, 5)
    add_filtered_param!(:kind, ['diff', 'graph'], 'diff')
    add_filtered_param!(:selection, selections.keys, 'main')
    add_filtered_param!(:job_ids, nil, nil)
    if params[:jobs].present?
      @job_ids = params[:jobs].select{|k,v| v == "1"}.keys
    end

    if !@job_ids || @job_ids.size < 2
      @diffs = []
      @info = "Select at least 2 jobs on previous page"
      return render
    end

    return if enforce_params!
    jobs = []
    @job_ids.each do |job|
      jobs << gitlab_artifact(job)
    end

    if jobs.size != @job_ids.size
      @info = "Failed to fetch some data"
      return render
    end

    sel = selections[@selection]

    executions = jobs.map do |job|
      all_executions(job[:data])
    end.flatten.uniq
    if executions.size > 1
      @executions = ['all'] + executions.to_a
      add_filtered_param!(:execution, :int, 'all')
      if @execution != 'all'
        sel[:include_execution] = @execution
      end
    end

    @jobs = jobs.map do |job|
      job.merge(res: process_rebench_data(job[:data], @warmup, sel))
    end


    @diffs = []
    suites = []
    @jobs.each do |job|
      suites += job[:res].keys
    end
    suites.uniq!

    if @kind == 'diff'
      @jobs.product(@jobs).each do |a,b|
        next if a == b
        a_prio =  artifact_job_names.keys.index(a[:info]["name"].to_sym)
        b_prio =  artifact_job_names.keys.index(b[:info]["name"].to_sym)
        next if a_prio > b_prio
        if a_prio == b_prio
          a_date = DateTime.parse(a[:info]["commit"]["committed_date"])
          b_date = DateTime.parse(b[:info]["commit"]["committed_date"])
          next if a_date < b_date
        end

        diff = {
          a: a,
          b: b,
          diff: {},
        }
        suites.each do |suite|
          res = diff_rebench_data(
              a[:res][suite] || {}, b[:res][suite] || {})
          diff[:diff][suite] = res if res
        end
        diff[:diff] = diff[:diff].sort{|a,b| a <=> b}
        means = diff[:diff].map do |_, suite|
          suite[:res].map do |_, r|
            r[:mean] unless r == :missing
          end
        end.flatten.compact
        medians = diff[:diff].map do |_, suite|
          suite[:res].map do |_, r|
            r[:median] unless r == :missing
          end
        end.flatten.compact
        diff[:diff] << ["summary", {
          res: [],
          median: gmean(medians),
          mean: gmean(means)
        }]
        @diffs << diff
      end
      respond_to do |format|
        format.yaml { send_data @diffs.to_yaml }
        format.html { render }
      end
    elsif @kind == 'graph'
      @graphs = {}
      @jobs.each do |job|
        logger.info job[:info]["id"]
        suites.each do |suite|
          @graphs[suite] ||= {}
          (job[:res][suite]||[]).each do |name, bm|
            @graphs[suite][name] ||= []
            @graphs[suite][name] << [job[:info]["id"], bm[:mean]]
          end
        end
      end
      @graphs = @graphs.sort{|a,b| a <=> b}
    end
  end

 private
  def ratio_to_class(ratio)
    if ratio == :missing
      'missing-data'
    elsif ratio >= 1.08
      'big-diff faster'
    elsif ratio >= 1.02
      'faster'
    elsif ratio <= 0.92
      'big-diff slower'
    elsif ratio <= 0.98
      'slower'
    else
      ''
    end
  end
  helper_method :ratio_to_class
  helper_method :artifact_job_names
end
