class ApplicationController < ActionController::Base
  def current_path
    required = @filtered_params.keys
    Hash[required.map{|r| [r, instance_variable_get("@#{r}")]}]
  end
  helper_method :current_path

  def enforce_params!
    required = @filtered_params.keys
    unless required.all? {|r| params[r].present?}
      redirect_to(current_path)
      return true
    end
    false
  end

  def add_filtered_param!(key, options, default)
    v = params[key]

    case options
    when :int
      v = (Integer(v) rescue default)
    when nil
    else
      unless options.include?(v)
        v = default
      end
    end

    instance_variable_set("@#{key}", v)
    @filtered_params ||= {}
    @filtered_params[key] = v
  end

  def add_params(form, skip)
    @filtered_params.map do |k,v|
      unless k == skip
        if v.is_a? Array
          v.map{|v|  form.hidden_field("#{k}[]",value: v)}.join(" ")
        else
          form.hidden_field(k,value: v)
        end
      end
    end.join("\n").html_safe
  end
  helper_method :add_params
end
