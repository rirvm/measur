class GraphController < ApplicationController
  include Repository
  include Artifacts
  include BenchmarkData

  def show
    add_filtered_param!(:warmup, :int, 5)
    add_filtered_param!(:branch, git_remote_branches, 'remotes/origin/master')
    add_filtered_param!(:vm, artifact_job_names.keys, artifact_job_names.keys.first)
    add_filtered_param!(:revisions, :int, 20)
    add_filtered_param!(:filter, ['all', 'merges'], 'all')
    return if enforce_params!

    @branches = git_remote_branches
    hashes = []
    Filelock repo_conf[:lock] do
      git_repo.checkout(@branch)
      if @filter == 'merges'
        hashes = git_repo.log((@revisions * 100))
                  .select{|c| c.message =~ /Merge pull request/}[0..@revisions]
      else
        hashes = git_repo.log(@revisions)
      end
    end

    jobs = hashes.map do |c|
      r = gitlab_artifacts_info(c, [@vm])
      r[@vm]['id'] if r[@vm] && r[@vm].any?
    end.compact

    res = jobs.map do |j|
      job = gitlab_artifact(j)
      job.merge(res: process_rebench_data(job[:data], @warmup))
    end

    @graphs = {}

    res.each do |job|
      job[:res].each do |suite, bms|
        bms.each do |bm, data|
          @graphs[suite] ||= {}
          @graphs[suite][bm] ||= []
          cm = job[:info]["commit"]
          @graphs[suite][bm] << [cm['short_id'], data[:mean], DateTime.parse(cm['committed_date'])]
        end
      end
    end

    @graphs.each do |suite, bms|
      bms.each do |name, data|
        @graphs[suite][name].sort!{|a,b| a[2] <=> b[2]}.map!{|d| [d[0], d[1]]}
      end
    end

    @graphs.each do |suite, bms|
      @graphs[suite] = bms.sort
    end
    @graphs = @graphs.sort{|a,b| a<=>b}
  end

  helper_method :artifact_job_names
end
