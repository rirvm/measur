Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get :revisions, to: 'revisions#index'
  get :diff, to: 'diff#show'
  get :graph, to: 'graph#show'
  get 'job/:job', to: 'data#job'

  root 'revisions#index'
end
